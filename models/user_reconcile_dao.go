/***************************************************
 ** @Desc : This file for 用户对账 计算
 ** @Time : 2019.05.29 15:34 
 ** @Author : Joker
 ** @File : user_reconcile_dao
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.05.29 15:34
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
	"recharge/models/utils"
	"recharge/sys"
)

var DBUtils = utils.GenerateSQL{}

// 查询条件
type UserReconcileDao struct {
	UserId      string `column:"and,user_id,="`
	EditTime    string `column:"and,edit_time,>="`
	EditTimeEnd string `column:"and,edit_time,<="`
	Status      string `column:"and,status,="`
}

// 计算结果
type Count struct {
	Total  float64 `orm:"column(total)"`
	Length int     `orm:"column(length)"`
}

// 计算成功转账金额
func (*UserReconcileDao) SelectCountSuccessTransfer(urd UserReconcileDao) (c Count) {
	om := orm.NewOrm()
	// 拼接where条件
	where, args := DBUtils.GenWhereByStruct(urd)
	// 总金额
	err := om.Raw("SELECT SUM(tr_amount) AS total,COUNT(0) AS length FROM transfer_record "+where, args).QueryRow(&c)
	if err != nil {
		sys.LogError("SelectCountSuccessTransfer failed to count for: ", err)
	}

	return
}

// 计算成功充值金额
func (*UserReconcileDao) SelectCountSuccessRecharge(urd UserReconcileDao) (c Count) {
	om := orm.NewOrm()
	//拼接where条件
	where, args := DBUtils.GenWhereByStruct(urd)
	err := om.Raw("SELECT SUM(re_amount) AS total,COUNT(0) AS length FROM recharge_record "+where, args).QueryRow(&c)
	if err != nil {
		sys.LogError("SelectCountSuccessRecharge failed to count for: ", err)
	}

	return
}

// 计算成功代付金额
func (*UserReconcileDao) SelectCountSuccessPay(urd UserReconcileDao) (c Count) {
	om := orm.NewOrm()
	//拼接where条件
	where, args := DBUtils.GenWhereByStruct(urd)
	err := om.Raw("SELECT SUM(wh_amount) AS total,COUNT(0) AS length FROM withdraw_record "+where, args).QueryRow(&c)
	if err != nil {
		sys.LogError("SelectCountSuccessPay failed to count for: ", err)
	}

	return
}
