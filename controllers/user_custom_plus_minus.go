/***************************************************
 ** @Desc : This file for 自定义加减款 模型控制
 ** @Time : 2019.05.22 15:15 
 ** @Author : Joker
 ** @File : user_custom_plus_minus
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.05.22 15:15
 ** @Software: GoLand
****************************************************/
package controllers

import (
	"recharge/models"
	"recharge/utils"
	"strconv"
	"strings"
)

type UCustomPlusMinus struct {
	KeepSession
}

var PMinusMdl  =models.CustomPlusMinus{}

// 加减款明细展示界面
// @router /merchant/list_pMinus/ [get]
func (the *UCustomPlusMinus) ListPlusMinusUI() {
	userName := the.GetSession("userName")
	if userName != nil {
		the.Data["userName"] = userName.(string)
	}

	right := the.GetSession("FilterRight")
	if right != nil {
		the.Data["right"] = right.(int)
	}

	the.Data["status"] = utils.GetOrderStatus()

	the.TplName = "custom_plus_minus.html"
}

// 加减款明细查询分页
// @router /merchant/list_plus_minus/?:params [get]
func (the *UCustomPlusMinus) QueryAndListPage() {
	//分页参数
	page, _ := strconv.Atoi(the.GetString("page"))
	limit, _ := strconv.Atoi(the.GetString("limit"))
	if limit == 0 {
		limit = 20
	}

	//查询参数
	in := make(map[string]interface{})
	MerchantName := strings.TrimSpace(the.GetString("_userName"))
	start := strings.TrimSpace(the.GetString("start"))
	end := strings.TrimSpace(the.GetString("end"))

	in["user_name"] = MerchantName

	if start != "" {
		in["edit_time__gte"] = start
	}
	if end != "" {
		in["edit_time__lte"] = end
	}

	//计算分页数
	count := PMinusMdl.SelectCustomPlusMinusCount(in)
	totalPage := count / limit // 计算总页数
	if count%limit != 0 { // 不满一页的数据按一页计算
		totalPage++
	}

	//数据获取
	var list []models.CustomPlusMinus
	if page <= totalPage {
		if page == totalPage {
			limit = count % limit
		}
		list = PMinusMdl.SelectCustomPlusMinusListPage(in, limit, (page-1)*limit)
	}

	//数据回显
	out := make(map[string]interface{})
	out["limit"] = limit //分页数据
	out["page"] = page
	out["totalPage"] = totalPage
	out["root"] = list //显示数据

	the.Data["json"] = out
	the.ServeJSON()
	the.StopRun()
}
